Gem::Specification.new do |s|
   s.name = %q{xml-magic}
   s.version = "0.1.2"
   s.date = %q{2011-02-11}
   s.authors = ["Anthony Crumley", "Ben Wyrosdick", "Sean Bamforth"]
   s.email = %q{anthony@commonthread.com}
   s.summary = %q{xml-magic makes accessing xml objects more like any other ruby object}
   s.homepage = %q{https://github.com/seanbamforth/xml_magic}
   s.has_rdoc = true
   s.description = %q{xml-magic makes accessing xml objects more like any other ruby object}
   s.files = [ "README.RDOC", "CHANGELOG", "LICENSE", "demo.rb", "lib/xml_magic.rb", "lib/common_thread/xml/xml_magic.rb", "test/test_helper.rb", "test/test_xml_magic.rb" ]
end